import { isEqual } from 'lodash'
import Selectors from './Selectors'

export default class CachedSelectors extends Selectors {
  constructor(props) {
    super(props)
    this.cache = {}
    this.doGet = Selectors.prototype.get.bind(this)
  }

  get(selector, state, extra) {
    const val = this.doGet(selector, state, extra)

    const key = `${selector}:${extra}`
    if (!isEqual(val, this.cache[key])) {
      this.cache[key] = val
      return val
    }

    return this.cache[key]
  }
}
