import { each } from 'lodash'

export default class ProviderRegistry {
  instances = {}

  started = false

  start(store) {
    this.store = store
    each(this.instances, (i, k) => {
      i.start()
      i.setStore(store)
    })

    this.started = true
  }

  register(key, instance) {
    this.instances[key] = instance

    if (this.started) {
      setTimeout(() => {
        instance.start()
        if (this.store) instance.setStore(this.store)
      }, 0)
    }
  }
}
