import { camelCase, upperFirst, each } from 'lodash'

export default class Actions {
  actions = []

  asyncActions = {}

  _actions = {}

  constructor(key) {
    this.key = key
  }

  setSelector(selector) {
    this.selector = selector
  }

  setStore(store) {
    this.store = store
  }

  slice(state) {
    return state[this.key]
  }

  start() {
    each(this.actions, (a) => this.createAction(a))
    each(this.asyncActions, (p, a) => this.createAsyncAction(a, p))
  }

  createAction(type) {
    this._actions[type] = {}
  }

  simpleAction(ty, payload) {
    return {
      type: ty,
      payload
    }
  }

  createAsyncAction(type, props) {
    this._actions[type] = {
      async: true,
      ...props
    }

    this._actions[`${type}_RESOLVED`] = {}
    this._actions[`${type}_REJECTED`] = {}
  }

  // https://github.com/civicsource/redux-rsi/blob/master/src/asyncify-action.js
  asyncifyAction(action) {
    return {
      request: action,
      completed: (response) => ({
        type: `${action.type}_RESOLVED`,
        payload: response,
        meta: action.payload // the original payload
      }),
      failed: (err) => ({
        type: `${action.type}_REJECTED`,
        payload: err,
        meta: action.payload, // the original payload
        error: true
      })
    }
  }

  asyncAction(type, payload) {
    const props = this._actions[type]
    const asyncAction = this.asyncifyAction(this.simpleAction(type, payload))

    return (dispatch, getState) => {
      const promise = props.promise(payload, this.selector(null, getState()))
      if (!promise) {
        console.log('No promise for', type, payload)
        return null
      }

      dispatch(asyncAction.request)
      return promise.then((response) => {
        // https://github.com/civicsource/redux-rsi/blob/master/src/create-ajax-action.js
        // (removed, breaks then chaining)
        dispatch(asyncAction.completed(response))

        // Allow building complex actions by calling another function after
        // this dispatch
        const fn = `dispatch${upperFirst(camelCase(type))}ResolvedAfter`
        if (this[fn]) this[fn](response)

        // for chaining
        return response
      }).catch((error) => {
        console.log('asyncAction error', error)
        let msg = error
        if (error.response) {
          if (error.response.status === 422) {
            msg = 'Invalid Fields: '
            each(error.response.data.description, (v, k) => { msg += `${k}: ${v}\n` })
          } else {
            msg = error.response.data.error
          }
        }

        dispatch(asyncAction.failed(msg))

        // Allow building complex actions by calling another function after
        // this dispatch
        const fn = `dispatch${upperFirst(camelCase(type))}RejectedAfter`
        if (this[fn]) this[fn](error)

        // for chaining
        throw error
      })
    }
  }

  dispatch(ty, payload) {
    if (!(ty in this._actions)) throw new Error(`${ty} not registered on ${this.key} Actions`)
    const fn = `dispatch${upperFirst(camelCase(ty))}`
    if (this[fn]) {
      return this.store.dispatch(this[fn](payload))
    }
    const action = this._actions[ty]
    if (action.async) return this.store.dispatch(this.asyncAction(ty, payload))
    return this.store.dispatch(this.simpleAction(ty, payload))
  }

  dispatchSimple(ty, payload) {
    if (!(ty in this._actions)) throw new Error(`${ty} not registered on ${this.key} Actions`)

    return this.store.dispatch(this.simpleAction(ty, payload))
  }
}
