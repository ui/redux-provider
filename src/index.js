import ProviderRegistry from './ProviderRegistry'
import ReducerRegistry from './ReducerRegistry'

export Selectors from './Selectors'
export CachedSelectors from './CachedSelectors'
export Actions from './Actions'
export Reducers from './Reducers'

export const reducerRegistry = new ReducerRegistry()
export const providerRegistry = new ProviderRegistry()

// Provider encapsulates all interactions with store
export class Provider {
  actions = null

  reducers = null

  selectors = null

  setStore(store) {
    this.store = store
    if (this.actions) this.actions.setStore(store)
  }

  constructor(key) {
    this.key = key
    providerRegistry.register(key, this)
  }

  // initialise the children classes and register the reducer
  // into the registry
  start() {
    if (this.actions) {
      this.actions = new this.actions(this.key)
      this.actions.start()
      this.actions.setSelector(this.selector)
    }
    if (this.reducers) this.reducers = new this.reducers(this.key)
    if (this.selectors) this.selectors = new this.selectors(this.key)

    // register this provider into the reducerRegistry
    reducerRegistry.register(this.key, this.reducer.bind(this), this)

    return this
  }

  // call a reducer
  reducer(state, action) {
    if (this.reducers) {
      return this.reducers.reducer(state, action)
    }
    return state
  }

  // get the store slice relating to this provider
  slice(state) {
    return state[this.key]
  }

  // get a selector from the current slice of the store
  selector = (selector, state, extra) => {
    if (this.selectors) {
      if (selector) {
        return this.selectors.get(selector, this.slice(state), extra)
      }
      return this.slice(state)
    }

    return null
  }

  // dispatch an action
  dispatch = (action, payload) => {
    if (this.actions) {
      return this.actions.dispatch(action, payload)
    }

    return null
  }
}
