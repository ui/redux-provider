import { values } from 'lodash'

export default class ReducerRegistry {
  reducers = {}

  onChange = null

  getReducers() {
    return values(this.reducers).length ? this.reducers : { default: () => ({}) }
  }

  register(key, reducer, instance) {
    this.reducers[key] = reducer
    if (this.onChange) this.onChange(this.getReducers())
  }

  setOnChange(fn) {
    this.onChange = fn
  }
}
