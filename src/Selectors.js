import {
  camelCase, upperFirst, get, isFunction
} from 'lodash'

export default class Selectors {
  selectors = {}

  constructor(key) {
    this.key = key
  }

  get(selector, state, extra) {
    if (!selector) return state[this.key]

    if (this.selectors[selector]) {
      if (isFunction(this.selectors[selector])) {
        return this.selectors[selector].call(this, state, extra)
      }

      return get(state, this.selectors[selector])
    }

    const fn = `select${upperFirst(camelCase(selector))}`
    if (this[fn]) {
      return this[fn](state)
    }

    throw new Error(`Cant find a selector ${selector} on ${this.key} Selector`)
  }
}
