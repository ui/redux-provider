import { camelCase, upperFirst } from 'lodash'
import Immutable from 'seamless-immutable'

export default class Reducers {
  initialState = {}

  merge(state, newState) {
    return Immutable.merge(state, newState)
  }

  mergeDeep(state, newState) {
    return Immutable.merge(state, newState, { deep: true })
  }

  setIn(state, path, value) {
    return Immutable.setIn(state, path, value)
  }

  without(object, key) {
    return Immutable.without(object, key)
  }

  immutable(object) {
    return Immutable.from(object)
  }

  reducer(state, action) {
    let _state = state
    if (!_state) _state = Immutable.from(this.initialState)

    const fn = `on${upperFirst(camelCase(action.type))}`
    if (this[fn]) {
      return this[fn](_state, action)
    }

    return _state
  }
}
