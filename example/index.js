import { providerRegistry } from '@esrf-ui/redux-provider'
import store from './store'
import myprovider from './myprovider'

providerRegistry.start(store)

myprovider.dispatch('TEST_ACTION')
