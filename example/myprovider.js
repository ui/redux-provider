/* eslint max-classes-per-file: 0 */
import {
  Provider, Reducers, Selectors, Actions
} from '@esrf-ui/redux-provider'

class MySelectors extends Selectors {
  selectors = {
    test: 'test'
  }
}

class MyReducers extends Reducers {
  initialState = {
    test: false
  }

  onTestAction(state) {
    return this.merge(state, { test: true })
  }
}

class MyActions extends Actions {
  actions = [
    'TEST_ACTION',
    'COMPLEX_ACTION'
  ]

  asyncActions = {
    TEST_ASYNC_ACTION: {
      promise: () => new Promise()
    }
  }

  onDispatchComplexAction() {
    this.dispatchSimple('COMPLEX_ACTION')

    // do stuff ...
    this.dispatch('TEST_ACTION')
  }
}

class MyProvider extends Provider {
  actions = MyActions

  reducers = MyReducers

  selectors = MySelectors
}

export default new MyProvider('test')
