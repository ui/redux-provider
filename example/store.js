import { combineReducers, createStore } from 'redux'
import { reducerRegistry } from '@esrf-ui/redux-provider'

const initialState = {}
const combine = (reducers) => {
  const reducerNames = Object.keys(reducers)
  Object.keys(initialState).forEach((item) => {
    if (reducerNames.indexOf(item) === -1) {
      reducers[item] = (state = null) => state
    }
  })
  return combineReducers(reducers)
}

const store = createStore(
  combine(reducerRegistry.getReducers())
)

reducerRegistry.setOnChange((reducers) => {
  store.replaceReducer(combine(reducers))
})

export default store
