# redux-provider

> Reducing redux action/reducer hell

[![NPM](https://img.shields.io/npm/v/redux-provider.svg)](https://www.npmjs.com/package/esrf-ui-redux-provider) [![Code Style](https://badgen.net/badge/code%20style/airbnb/ff5a5f?icon=airbnb)](https://github.com/airbnb/javascript)

An effort to reduce redux boilerplate in react/redux apps. 
* Keeps actions, reducers, (and selectors) in the same file for logical organisation
* Helpers for async actions, that automatically dispatch act_RESOLVED and act_REJECTED
* Uses seamless-immutable to avoid accidental state mutation
* Inverts control of the store, the provider decides where it lives within the store
* Dynamic provider initialisation, if a provider isnt loaded, it never pollutes the store
* Can cache selectors


## Install

```bash
npm install --save @esrf-ui/redux-provider
```

## Usage

```js
import { 
  Provider, Reducers, Selectors, Actions 
} from '@esrf-ui/redux-provider'

class MySelectors extends Selectors {
  selectors = {
    test: 'test'
  }
}

class MyReducers extends Reducers {
  initialState = {
    test: false
  }

  onTestAction(state) {
    return this.merge(state, { test: true })
  }
}

class MyActions extends Actions {
  actions = [
    'TEST_ACTION',
    'COMPLEX_ACTION'
  ]

  asyncActions = {
    TEST_ASYNC_ACTION: {
      promise: () => new Promise()
    }
  }

  onDispatchComplexAction() {
    this.dispatchSimple('COMPLEX_ACTION')

    // do stuff ...
    this.dispatch('TEST_ACTION')
  }
}

class MyProvider extends Provider {
  actions = MyActions

  reducers = MyReducers
  
  selectors = MySelectors
}

// initialise provider with selected position in store
// all above initialState will exist under store.getState().test
const myprovider = new MyProvider('test')
```

```js
const mapDispatchToProps = (dispatch) => {
  return {
    actions: {
      removeAlert:  (payload) => {
        myprovider.dispatch('TEST_ACTION', payload)
      }
    }
  }
}

const mapStateToProps = (state) => {
  return {
    alerts: myprovider.selector('test', state)
  }
}

const connected = connect(mapStateToProps, mapDispatchToProps)(Component)
```

### Cached Selectors
There is a also a `CachedSelectors` version of `Selectors` that will cache values between selections. 
This should reduce the number of renders with object comparison as it uses lodashes `isEqual` instead of `===`

```js
import { 
  CachedSelectors 
} from '@esrf-ui/redux-provider'

class MySelectors extends CachedSelectors {
  selectors = {
    test: 'test'
  }
}
```

## License

MIT © [ESRF](https://gitlab.esrf.fr/ui/redux-provider)
