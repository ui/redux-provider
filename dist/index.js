'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var lodash = require('lodash');
var Immutable = _interopDefault(require('seamless-immutable'));

var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

var _extends = Object.assign || function (target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i];

    for (var key in source) {
      if (Object.prototype.hasOwnProperty.call(source, key)) {
        target[key] = source[key];
      }
    }
  }

  return target;
};

var inherits = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
};

var possibleConstructorReturn = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
};

var ProviderRegistry = function () {
  function ProviderRegistry() {
    classCallCheck(this, ProviderRegistry);
    this.instances = {};
    this.started = false;
  }

  createClass(ProviderRegistry, [{
    key: 'start',
    value: function start(store) {
      this.store = store;
      lodash.each(this.instances, function (i, k) {
        i.start();
        i.setStore(store);
      });

      this.started = true;
    }
  }, {
    key: 'register',
    value: function register(key, instance) {
      var _this = this;

      this.instances[key] = instance;

      if (this.started) {
        setTimeout(function () {
          instance.start();
          if (_this.store) instance.setStore(_this.store);
        }, 0);
      }
    }
  }]);
  return ProviderRegistry;
}();

var ReducerRegistry = function () {
  function ReducerRegistry() {
    classCallCheck(this, ReducerRegistry);
    this.reducers = {};
    this.onChange = null;
  }

  createClass(ReducerRegistry, [{
    key: 'getReducers',
    value: function getReducers() {
      return lodash.values(this.reducers).length ? this.reducers : { default: function _default() {
          return {};
        } };
    }
  }, {
    key: 'register',
    value: function register(key, reducer, instance) {
      this.reducers[key] = reducer;
      if (this.onChange) this.onChange(this.getReducers());
    }
  }, {
    key: 'setOnChange',
    value: function setOnChange(fn) {
      this.onChange = fn;
    }
  }]);
  return ReducerRegistry;
}();

var Selectors = function () {
  function Selectors(key) {
    classCallCheck(this, Selectors);
    this.selectors = {};

    this.key = key;
  }

  createClass(Selectors, [{
    key: 'get',
    value: function get$$1(selector, state, extra) {
      if (!selector) return state[this.key];

      if (this.selectors[selector]) {
        if (lodash.isFunction(this.selectors[selector])) {
          return this.selectors[selector].call(this, state, extra);
        }

        return lodash.get(state, this.selectors[selector]);
      }

      var fn = 'select' + lodash.upperFirst(lodash.camelCase(selector));
      if (this[fn]) {
        return this[fn](state);
      }

      throw new Error('Cant find a selector ' + selector + ' on ' + this.key + ' Selector');
    }
  }]);
  return Selectors;
}();

var CachedSelectors = function (_Selectors) {
  inherits(CachedSelectors, _Selectors);

  function CachedSelectors(props) {
    classCallCheck(this, CachedSelectors);

    var _this = possibleConstructorReturn(this, (CachedSelectors.__proto__ || Object.getPrototypeOf(CachedSelectors)).call(this, props));

    _this.cache = {};
    _this.doGet = Selectors.prototype.get.bind(_this);
    return _this;
  }

  createClass(CachedSelectors, [{
    key: 'get',
    value: function get$$1(selector, state, extra) {
      var val = this.doGet(selector, state, extra);

      var key = selector + ':' + extra;
      if (!lodash.isEqual(val, this.cache[key])) {
        this.cache[key] = val;
        return val;
      }

      return this.cache[key];
    }
  }]);
  return CachedSelectors;
}(Selectors);

var Actions = function () {
  function Actions(key) {
    classCallCheck(this, Actions);
    this.actions = [];
    this.asyncActions = {};
    this._actions = {};

    this.key = key;
  }

  createClass(Actions, [{
    key: 'setSelector',
    value: function setSelector(selector) {
      this.selector = selector;
    }
  }, {
    key: 'setStore',
    value: function setStore(store) {
      this.store = store;
    }
  }, {
    key: 'slice',
    value: function slice(state) {
      return state[this.key];
    }
  }, {
    key: 'start',
    value: function start() {
      var _this = this;

      lodash.each(this.actions, function (a) {
        return _this.createAction(a);
      });
      lodash.each(this.asyncActions, function (p, a) {
        return _this.createAsyncAction(a, p);
      });
    }
  }, {
    key: 'createAction',
    value: function createAction(type) {
      this._actions[type] = {};
    }
  }, {
    key: 'simpleAction',
    value: function simpleAction(ty, payload) {
      return {
        type: ty,
        payload: payload
      };
    }
  }, {
    key: 'createAsyncAction',
    value: function createAsyncAction(type, props) {
      this._actions[type] = _extends({
        async: true
      }, props);

      this._actions[type + '_RESOLVED'] = {};
      this._actions[type + '_REJECTED'] = {};
    }

    // https://github.com/civicsource/redux-rsi/blob/master/src/asyncify-action.js

  }, {
    key: 'asyncifyAction',
    value: function asyncifyAction(action) {
      return {
        request: action,
        completed: function completed(response) {
          return {
            type: action.type + '_RESOLVED',
            payload: response,
            meta: action.payload // the original payload
          };
        },
        failed: function failed(err) {
          return {
            type: action.type + '_REJECTED',
            payload: err,
            meta: action.payload, // the original payload
            error: true
          };
        }
      };
    }
  }, {
    key: 'asyncAction',
    value: function asyncAction(type, payload) {
      var _this2 = this;

      var props = this._actions[type];
      var asyncAction = this.asyncifyAction(this.simpleAction(type, payload));

      return function (dispatch, getState) {
        var promise = props.promise(payload, _this2.selector(null, getState()));
        if (!promise) {
          console.log('No promise for', type, payload);
          return null;
        }

        dispatch(asyncAction.request);
        return promise.then(function (response) {
          // https://github.com/civicsource/redux-rsi/blob/master/src/create-ajax-action.js
          // (removed, breaks then chaining)
          dispatch(asyncAction.completed(response));

          // Allow building complex actions by calling another function after
          // this dispatch
          var fn = 'dispatch' + lodash.upperFirst(lodash.camelCase(type)) + 'ResolvedAfter';
          if (_this2[fn]) _this2[fn](response);

          // for chaining
          return response;
        }).catch(function (error) {
          console.log('asyncAction error', error);
          var msg = error;
          if (error.response) {
            if (error.response.status === 422) {
              msg = 'Invalid Fields: ';
              lodash.each(error.response.data.description, function (v, k) {
                msg += k + ': ' + v + '\n';
              });
            } else {
              msg = error.response.data.error;
            }
          }

          dispatch(asyncAction.failed(msg));

          // Allow building complex actions by calling another function after
          // this dispatch
          var fn = 'dispatch' + lodash.upperFirst(lodash.camelCase(type)) + 'RejectedAfter';
          if (_this2[fn]) _this2[fn](error);

          // for chaining
          throw error;
        });
      };
    }
  }, {
    key: 'dispatch',
    value: function dispatch(ty, payload) {
      if (!(ty in this._actions)) throw new Error(ty + ' not registered on ' + this.key + ' Actions');
      var fn = 'dispatch' + lodash.upperFirst(lodash.camelCase(ty));
      if (this[fn]) {
        return this.store.dispatch(this[fn](payload));
      }
      var action = this._actions[ty];
      if (action.async) return this.store.dispatch(this.asyncAction(ty, payload));
      return this.store.dispatch(this.simpleAction(ty, payload));
    }
  }, {
    key: 'dispatchSimple',
    value: function dispatchSimple(ty, payload) {
      if (!(ty in this._actions)) throw new Error(ty + ' not registered on ' + this.key + ' Actions');

      return this.store.dispatch(this.simpleAction(ty, payload));
    }
  }]);
  return Actions;
}();

var Reducers = function () {
  function Reducers() {
    classCallCheck(this, Reducers);
    this.initialState = {};
  }

  createClass(Reducers, [{
    key: 'merge',
    value: function merge(state, newState) {
      return Immutable.merge(state, newState);
    }
  }, {
    key: 'mergeDeep',
    value: function mergeDeep(state, newState) {
      return Immutable.merge(state, newState, { deep: true });
    }
  }, {
    key: 'setIn',
    value: function setIn(state, path, value) {
      return Immutable.setIn(state, path, value);
    }
  }, {
    key: 'without',
    value: function without(object, key) {
      return Immutable.without(object, key);
    }
  }, {
    key: 'immutable',
    value: function immutable(object) {
      return Immutable.from(object);
    }
  }, {
    key: 'reducer',
    value: function reducer(state, action) {
      var _state = state;
      if (!_state) _state = Immutable.from(this.initialState);

      var fn = 'on' + lodash.upperFirst(lodash.camelCase(action.type));
      if (this[fn]) {
        return this[fn](_state, action);
      }

      return _state;
    }
  }]);
  return Reducers;
}();

var reducerRegistry = new ReducerRegistry();
var providerRegistry = new ProviderRegistry();

// Provider encapsulates all interactions with store
var Provider = function () {
  createClass(Provider, [{
    key: 'setStore',
    value: function setStore(store) {
      this.store = store;
      if (this.actions) this.actions.setStore(store);
    }
  }]);

  function Provider(key) {
    var _this = this;

    classCallCheck(this, Provider);
    this.actions = null;
    this.reducers = null;
    this.selectors = null;

    this.selector = function (selector, state, extra) {
      if (_this.selectors) {
        if (selector) {
          return _this.selectors.get(selector, _this.slice(state), extra);
        }
        return _this.slice(state);
      }

      return null;
    };

    this.dispatch = function (action, payload) {
      if (_this.actions) {
        return _this.actions.dispatch(action, payload);
      }

      return null;
    };

    this.key = key;
    providerRegistry.register(key, this);
  }

  // initialise the children classes and register the reducer
  // into the registry


  createClass(Provider, [{
    key: 'start',
    value: function start() {
      if (this.actions) {
        this.actions = new this.actions(this.key);
        this.actions.start();
        this.actions.setSelector(this.selector);
      }
      if (this.reducers) this.reducers = new this.reducers(this.key);
      if (this.selectors) this.selectors = new this.selectors(this.key);

      // register this provider into the reducerRegistry
      reducerRegistry.register(this.key, this.reducer.bind(this), this);

      return this;
    }

    // call a reducer

  }, {
    key: 'reducer',
    value: function reducer(state, action) {
      if (this.reducers) {
        return this.reducers.reducer(state, action);
      }
      return state;
    }

    // get the store slice relating to this provider

  }, {
    key: 'slice',
    value: function slice(state) {
      return state[this.key];
    }

    // get a selector from the current slice of the store


    // dispatch an action

  }]);
  return Provider;
}();

exports.Selectors = Selectors;
exports.CachedSelectors = CachedSelectors;
exports.Actions = Actions;
exports.Reducers = Reducers;
exports.reducerRegistry = reducerRegistry;
exports.providerRegistry = providerRegistry;
exports.Provider = Provider;
//# sourceMappingURL=index.js.map
